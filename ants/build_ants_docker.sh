#!/bin/bash

function showHelp {
    echo "================ USAGE ================"
    echo "build_ants_docker.sh [options] version"
    echo "required:"
    echo "version               ants version tag to build (or commit tag if --build-commit)"
    echo " "
    echo "options:"
    echo "-h, --help            show help"
    echo "--build-threads N     build using N threads"
    echo "--build-commit        build a specific commit tag instead of tagged version"
    echo "--build-shared        build ANTs using shared libraries"
    echo "--build-with-vtk      build ANTs using VTK instead of ITK"
    
}

N_CPU=1
DOCKER_BASE=debian
ANTS_BUILD_COMMIT=false
ANTS_BUILD_SHARED_FLAG=false
ANTS_BUILD_VTK_FLAG=false

export ANTS_VERSION=
export ANTS_COMMIT_HASH=
export ANTS_BUILD_SHARED=OFF
export ANTS_BUILD_VTK=OFF

if hash greadlink 2>/dev/null; then READLINK=greadlink; else READLINK=readlink; fi

while test $# -gt 0; do
    case "$1" in
        -h|--help)
            showHelp
            exit 0
            ;;
        --build-threads)
            shift
            N_CPU=$1
            shift
            ;;
        --base)
            shift
            DOCKER_BASE=$1
            shift
            ;;
        --build-commit)
            ANTS_BUILD_COMMIT=true
            shift
            ;;
        --build-shared)
            ANTS_BUILD_SHARED_FLAG=true
            shift
            ;;
        --build-with-vtk)
            ANTS_BUILD_VTK_FLAG=true
            shift
            ;;
        -*)
            echo "Unrecognized option: $1"
            echo " "
            showHelp
            exit 1
            ;;
        *)
            break
            ;;
    esac
done

if [ $DOCKER_BASE == 'centos' ]; then
    DOCKERFILE_PATH=$(dirname $($READLINK -f $0))/Dockerfile-centos
else
    DOCKERFILE_PATH=$(dirname $($READLINK -f $0))/Dockerfile
fi

if $ANTS_BUILD_COMMIT; then export ANTS_COMMIT_HASH=$1; else export ANTS_VERSION=$1; fi
if $ANTS_BUILD_SHARED_FLAG; then export ANTS_BUILD_SHARED=ON; fi
if $ANTS_BUILD_VTK_FLAG; then export ANTS_BUILD_VTK=ON; fi

export N_BUILD_THREADS=${N_CPU}

docker build --build-arg N_BUILD_THREADS --build-arg ANTS_COMMIT_HASH --build-arg ANTS_VERSION --build-arg ANTS_BUILD_SHARED --build-arg ANTS_BUILD_VTK -f $DOCKERFILE_PATH .